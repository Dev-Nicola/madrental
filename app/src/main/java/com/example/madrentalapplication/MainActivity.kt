package com.example.madrentalapplication

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_car.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {
    private var imageView: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        list_cars.setHasFixedSize(true)

        val layoutManager = LinearLayoutManager(this)
        list_cars.layoutManager = layoutManager

        val listCars: MutableList<Car> = ArrayList()

        val url = "http://s519716619.onlinehome.fr/exchange/madrental/images/zoom-buggy.jpg"
        val picasso = Picasso.get()

        val service = RetrofitSingleton.retrofit.create(WSInterface::class.java)
        val call = service.getCars()
        call.enqueue(object : Callback<List<ReturnWSGet>> {
            override fun onResponse(call: Call<List<ReturnWSGet>>, response: Response<List<ReturnWSGet>>) {
                if (response.isSuccessful) {
                    val returnWSGet = response.body()

                    if (returnWSGet != null) {
                        for (car in returnWSGet)
                            listCars.add(Car(
                                car.nom,
                                car.prixjournalierbase.toString() + " € / jour",
                                "Catégorie CO2 :" + car.categorieco2))

                        val carsAdapter = CarsAdapter(listCars)
                        list_cars.adapter = carsAdapter
                    }
                }
            }

            override fun onFailure(call: Call<List<ReturnWSGet>>, t: Throwable) {
                Log.e(
                    "tag",
                    "${t.message}"
                )
            }
        })

    }
}